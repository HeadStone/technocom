﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static int _schemaIndex;
        static int _deviceIndex;
        static void Main()
        {

            var token = GetToken();

            var schemas = GetSchemas(token);

            var devices = GetDevices(schemas, token);

            Console.WriteLine("Enter start date in yyyyMMdd or yyyyMMdd-HHmm format");
            var startDate = Console.ReadLine();
            
            Console.WriteLine("Enter end date in yyyyMMdd or yyyyMMdd-HHmm format");
            var endDate = Console.ReadLine();
            
            var tripsUrl = $"http://m.tk-chel.ru/ServiceJSON/GetTrips?schemaID={schemas[_schemaIndex].Id}&IDs={devices.Items[_deviceIndex].Id}&SD={startDate}&ED={endDate}&tripSplitterIndex=0";
            var tripsJson = Request(tripsUrl, token);

            double totalDistance = 0;
            dynamic trips = JObject.Parse(tripsJson);
            foreach (dynamic trip in trips[devices.Items[_deviceIndex].Id.ToString()].Trips)
            {
                totalDistance += Double.Parse(trip.Total["TotalDistance"].ToString());
            }

            Console.WriteLine();
            Console.WriteLine($"Total distance = {totalDistance}");
        }

        private static Devices GetDevices(List<Schema> schemas, string token)
        {
            var devicesUrl = $"http://m.tk-chel.ru/ServiceJSON/EnumDevices/?schemaID={schemas[_schemaIndex].Id}";
            Devices devices = JsonConvert.DeserializeObject<Devices>(Request(devicesUrl, token));


            for (var index = 0; index < devices.Items.Length; index++)
            {
                var item = devices.Items[index];
                Console.WriteLine($"{index} {item.Name} {item.Serial}");
            }

            Console.WriteLine("Enter device index");
            _deviceIndex = Convert.ToInt32(Console.ReadLine());
            return devices;
        }

        private static List<Schema> GetSchemas(string token)
        {
            List<Schema> schemas =
                JsonConvert.DeserializeObject<List<Schema>>(Request("http://m.tk-chel.ru/ServiceJSON/EnumSchemas",
                    token));


            for (var index = 0; index < schemas.Count; index++)
            {
                var schema = schemas[index];
                Console.WriteLine($"{index} {schema.Name}");
            }

            Console.WriteLine("Enter schema index");
            _schemaIndex = Convert.ToInt32(Console.ReadLine());
            return schemas;
        }

        private static string GetToken()
        {
            string token;
            using (var client = new WebClient())
            {
                var url = "http://m.tk-chel.ru/ServiceJSON/Login";
                var json = JsonConvert.SerializeObject(new {UserName = "demo", Password = "demo"});
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                token = client.UploadString(url, "POST", json);
            }

            return token;
        }

        private static string Request(string url, string token)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("AG-TOKEN", token);
                return client.UploadString(url, "GET");
            }
        }
    }
}