﻿using System;

namespace ConsoleApp1
{
    public class GroupItem
    {
        public Guid Id { get; set; }             
        public string Name { get; set; }
    }
}